package me.dueris.genesismc.factory.conditions;


import me.dueris.genesismc.factory.conditions.biEntity.BiEntityCondition;
import me.dueris.genesismc.factory.conditions.biome.BiomeCondition;
import me.dueris.genesismc.factory.conditions.block.BlockCondition;
import me.dueris.genesismc.factory.conditions.damage.DamageCondition;
import me.dueris.genesismc.factory.conditions.entity.EntityCondition;
import me.dueris.genesismc.factory.conditions.fluid.FluidCondition;
import me.dueris.genesismc.factory.conditions.item.ItemCondition;
import org.bukkit.Bukkit;

import java.util.ArrayList;

public class CraftCondition {
    public static BiEntityCondition bientity;
    public static BiomeCondition biome;
    public static BlockCondition blockCon;
    public static DamageCondition damage;
    public static EntityCondition entity;
    public static FluidCondition fluidCon;
    public static ItemCondition item;
    // Depreciated lol
    // public static List<Class<? extends Condition>> findCraftConditionClasses() throws IOException {
    //     List<Class<? extends Condition>> classes = new ArrayList<>();
    //     Reflections reflections = new Reflections("me.dueris.genesismc.factory.conditions");

    //     Set<Class<? extends Condition>> subTypes = reflections.getSubTypesOf(Condition.class);
    //     for (Class<? extends Condition> subType : subTypes) {
    //         if (!subType.isInterface() && !subType.isEnum()) {
    //             classes.add(subType);
    //         }
    //     }

    //     return classes;
    // }
    protected static ArrayList<Class<? extends Condition>> customConditions = new ArrayList<>();

    public static void registerCustomCondition(Class<? extends Condition> condition) {
        customConditions.add(condition);
        Bukkit.getLogger().info("Origins Condition[%c] has been registered!".replace("%c", condition.getName()));
    }
}
